#In Jenkins:

New item -> Name: 'sample-paac' -> Pipeline + OK
Advanced Project Options
	Pipeline
		Definition: Pipeline script
		Script:
		 pipeline {
			agent any
			stages {
				stage ('Fetch code') {
					steps {
						git branch: 'paac', url: 'https://github.com/devopshydclub/vprofile-project.git'
					}
				}
				stage('Build') {
					steps {
						sh 'mvn install'
					}
				}
				stage('Test'){
					steps {
						sh 'mvn test'
					}
				}
			}
		}
        
SAVE

Build Now

#Every build has its own workspace

You click on 'build_ID' -> Workspaces -> You find here the path that you can click on -> target -> vprofile-v2.war
