#!/bin/bash

# How to install Jenkins on EC2 Instance:
#
# Check the website: www.jenkins.io
#
# 1. Install dependencies:

sudo yum update 
sudo yum install java-1.8.0-openjdk-devel -y
sudo yum install maven -y

# 2. Add Jenkins Software Repository:

sudo wget –O /etc/yum.repos.d/jenkins.repo http://pkg.jenkins-ci.org/redhat-stable/jenkins.repo

# The system will reach out to the Jenkins server and download the location of the repository to your system.
# It should display /etc/yum.repos.d/jenkins.repo saved.
# 
# Adding the repository manually – In some instances, the repository will not download correctly. To manually 
# add the repository, enter the following:
#
# sudo nano /etc/yum.repos.d/jenkins.repo
#
# This will open the jenkins.repo file for editing. Enter the following lines:
# --------------------------------------------------------------------
# [jenkins]
#
# name=Jenkins-stable
#
# baseurl=http://pkg.jenkins.io/redhat
# 
# gpgcheck=1
# --------------------------------------------------------------------
# Next, import the GPG key to ensure your software is legitimate:
#
# sudo rpm ––import https://pkg.jenkins.io/redhat/jenkins.io.key

# 3. Install Jenkins on CentOS

sudo yum install jenkins

sudo systemctl start jenkins
sudo systemctl enable jenkins

# 4. Set Firewall to Allow Jenkins

sudo firewall-cmd ––permanent ––zone=public ––add-port=8080/tcp
sudo firewall-cmd ––reload

# The home directory of Jenkins: cd /var/lib/jenkins/

# The password file : cat /var/lib/jenkins/secrets/initialAdminPassword
