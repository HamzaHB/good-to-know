#Once creating and attaching the EBS to our instance:

#We connect to our instance :
-----------------------------

sudo -i

cd /var/www/html/   #to acceed to our website files


# We want to put for example the image folder in a separate storage :
---------------------------------------------------------------------

fdisk -l            #to list all your disks (one is mounted by default and we can see the new EBS not mounted)

df -h               #to see where the disks are mounted to


# We have to create a disk partition in our new EBS :
-----------------------------------------------------

fdisk /dev/xvdf     #for the new EBS
  
m                   #for help

n                   #to add new partition

p                   #for primary partition

1                   #for partition number, if there is no partition we enter 1

enter               #to choose a default sector

enter               #to create a partition for the entire disk, we can put +3G for example

p                   #to print the partition

w                   #to write

fdisk -l            #to verify if the partition was well created


#The next part is doing format :
--------------------------------

mkfs (+tab 2 times) #to see the available utilities and we'll use the mkfs.ext4

mkfs.ext4 /dev/xvdf1  (Now the partition is created with the extation 4 format) we can use mkfs.xfs also



#Now the format is done, it's time to mount it : (We want to mount it to images)
------------------------------------------------

cd /var/www/html/
ls
ls img
mkdir /tmp/img-backups
mv img/* /tmp/img-backups/


#Now it's time to mount, let's first do a temparary mount (if we reboot the machine we'll not find the mount) :
---------------------------------------------------------------------------------------------------------------

cd
mount /dev/xvdf1 /var/www/html/img/
df -h
umount /var/www/html/img/  (to unmount)
df -h


#Let's now do a permanantly mount :
-----------------------------------

vi /etc/fstab
#We add at the end of the file:
/dev/xvdf1      /var/www/html/img       ext4    defaults        0 0

mount -a           #(to see if there is no error)
df -h
mv /tmp/img-backups/* /var/www/html/img/
systemctl restart httpd
systemctl status httpd

ls /var/www/html/img/

#We can test the website now, if the images not found, we have to disable the selinux 

vi /etc/selinux/config
we change (enforcing) to (disabled)
